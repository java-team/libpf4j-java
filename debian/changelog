libpf4j-java (3.12.0+dfsg-1) unstable; urgency=medium

  * New upstream version 3.12.0+dfsg
  * Refreshing patches
  * Updating copyright years
  * Raising Standards version to 4.7.0
  * Adding a versioned B-D on libsemver-java
  * Repacking with the gradle dir but without its jar
  * Skipping one more test file that requires google.testing.compile classes
  * Skipping a test that requires unpackaged mockito classes
  * Ignoring more unpackaged Maven artifacts

 -- Pierre Gruet <pgt@debian.org>  Mon, 30 Sep 2024 23:31:16 +0200

libpf4j-java (3.10.0+dfsg-1) unstable; urgency=medium

  * New upstream version 3.10.0+dfsg
  * Refreshing patches
  * Bringing in some classes needed for the tests thanks to libguava-java
  * Updating the patch to skip tests relying on unpackaged
    com.google.testing.compile

 -- Pierre Gruet <pgt@debian.org>  Tue, 13 Feb 2024 22:25:27 +0100

libpf4j-java (3.9.0+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Add upstream patch to address:
    CVE-2023-40826 CVE-2023-40827 CVE-2023-40828 (Closes: #1050834)

 -- tony mancill <tmancill@debian.org>  Wed, 30 Aug 2023 15:18:17 -0700

libpf4j-java (3.9.0+dfsg-1) unstable; urgency=medium

  * New upstream version 3.9.0+dfsg
  * Removing an unused Lintian override

 -- Pierre Gruet <pgt@debian.org>  Sun, 18 Jun 2023 15:24:25 +0200

libpf4j-java (3.8.0+dfsg-2) unstable; urgency=medium

  * Stopping requiring the org.objectweb.asm module (Closes: #1026673)
  * Raising Standards version to 4.6.2 (no change)

 -- Pierre Gruet <pgt@debian.org>  Tue, 20 Dec 2022 22:00:35 +0100

libpf4j-java (3.8.0+dfsg-1) unstable; urgency=medium

  * New upstream version 3.8.0+dfsg
  * Simplifying d/maven.rules
  * Adding a Lintian override for the bad-jar-name warning

 -- Pierre Gruet <pgt@debian.org>  Tue, 15 Nov 2022 22:27:25 +0100

libpf4j-java (3.7.0+dfsg-1) unstable; urgency=medium

  * New upstream version 3.7.0+dfsg
  * Refreshing patches
  * Raising Standards version to 4.6.1 (no change)
  * Providing https form of Homepage in d/control
  * Adding B-D on libtemplating-maven-plugin-java

 -- Pierre Gruet <pgt@debian.org>  Mon, 04 Jul 2022 22:38:18 +0200

libpf4j-java (3.6.0+dfsg-3) unstable; urgency=medium

  * Correcting d/watch to get the latest version from tags
  * Raising Standards version to 4.6.0 (no change)

 -- Pierre Gruet <pgt@debian.org>  Mon, 20 Dec 2021 11:42:48 +0100

libpf4j-java (3.6.0+dfsg-2) unstable; urgency=medium

  * Source-only upload

 -- Pierre Gruet <pgt@debian.org>  Tue, 17 Aug 2021 18:02:40 +0200

libpf4j-java (3.6.0+dfsg-1) experimental; urgency=medium

  * Initial release (Closes: #988279)

 -- Pierre Gruet <pgt@debian.org>  Tue, 11 May 2021 09:13:39 +0200
